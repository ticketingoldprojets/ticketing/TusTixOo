odoo.define('point_of_sale_tour.db', function (require) {
    "use strict";

    var core = require('web.core');
    var _t = core._t;

    var PosDB = require('point_of_sale.DB');
    var PosDB = PosDB.include({
        get_tour_category_list: function () {
            // Para el DEMO tomaremos valores fijos, pero en la versión final se le debe solicitar al BackEnd
            return [
                    {
                        id: 1,
                        name: 'Maritime Shuttles',
                        childs: [
                            {id: 11, name: 'Arcachon - Cap Ferret'},
                            {id: 12, name: 'LE MOULLEAU - CAP FERRET'},
                            {id: 13, name: 'Le Canon - Arcachon'},
                            {id: 14, name: 'Andernos - Arcachon'},
                            {id: 15, name: 'Dune du Pilat - Cap Ferret'},
                            {id: 16, name: "Dune du Pilat - Banc d' Arguin"},
                        ]
                    },
                    {
                        id: 2,
                        name: 'Excursions',
                        childs: [
                            {id: 21, name: 'Boating around the Bird Island'},
                            {id: 22, name: 'GRAND TOUR OF THE BAY'},
                            {id: 23, name: 'Boating towards the Bird Island'}]
                    },
                    // {
                    //     id: 3,
                    //     name: 'SCHOOLS',
                    //     childs: [
                    //         {id: 31, name: 'The apprentice sailor school day'},
                    //         {id: 32, name: 'Trip towards the birds Island'}]
                    // },
                ];
        },
        get_tour_price : function(tour_category_id, product_id, boarding_from, boarding_to, boarding_time){
            // Para el DEMO el precio se va a fijar, pero en la versión final se le debe solicitar al BackEnd
            var prices = {
                11: {2: 7.5, 3:5.0, 4:3.0, 5: 2.0, 6: 6.0, 7: 10.0},
                12: {2: 7.5, 3:5.0, 4:3.0, 5: 2.0, 6: 6.0, 7: 10.0},
                13: {2: 7.5, 3:5.0, 4:3.0, 5: 2.0, 6: 6.0, 7: 10.0},
                14: {2: 7.5, 3:5.0, 4:3.0, 5: 2.0, 6: 6.0, 7: 10.0},
                15: {2: 7.5, 3:5.0, 4:3.0, 5: 2.0, 6: 6.0, 7: 10.0},
                16: {2: 8.5, 3:6.0, 4:3.0, 5: 2.0, 6: 6.0, 7: 10.0},
                21: {2: 12.5, 3:5.0, 4:3.0, 5: 2.0},
                22: {2: 25.0, 3:17.0, 4:9.0, 5: 2.0},
                23: {2: 12.5, 3:9.0, 4:5.0, 5: 2.0},
                // 31: {2: 7.5, 3:5.0, 4:3.0, 5: 2.0, 6: 6.0, 7: 10.0},
                // 32: {2: 7.5, 3:5.0, 4:3.0, 5: 2.0, 6: 6.0, 7: 10.0},
            };

            return prices[tour_category_id] && prices[tour_category_id][product_id] || 0.0;
        },
        get_tour_category_boardings_by_id: function(categ_id){
            // Para el DEMO tomaremos valores fijos, pero en la versión final se le debe solicitar al BackEnd
            var defaul_boardings = [{ label: _t('Arcachon') ,  item: 'arcachon' }];
            var boardings = {
                11: [{ label: _t('Arcachon') ,  item: 'arcachon' },
                     { label: _t('Cap Ferret') ,  item: 'cap_ferret' }],
                12: [{ label: _t('Le Moulleau') ,  item: 'le_moulleau' },
                     { label: _t('Cap Ferret') ,  item: 'cap_ferret' }],
                13: [{ label: _t('Le Cannon') ,  item: 'le_cannon' },
                     { label: _t('Arcachon') ,  item: 'arcachon' }],
                14: [{ label: _t('Andernos') ,  item: 'andernos' },
                     { label: _t('Arcachon') ,  item: 'arcachon' }],
                15: [{ label: _t('Dune du Pilat') ,  item: 'dune_du_pilat' },
                     { label: _t('Cap Ferret') ,  item: 'cap_ferret' }],
                16: [{ label: _t('Dune du Pilat') ,  item: 'dune_du_pilat' },
                     { label: _t("Banc d' Arguin") ,  item: 'banc_d_arguin' }],
            };
            return boardings[categ_id] || defaul_boardings;
        },
    });
});