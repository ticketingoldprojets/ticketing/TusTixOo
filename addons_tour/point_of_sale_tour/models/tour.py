# -*- coding: utf-8 -*-

from odoo import models, fields, _

class Tour(models.Model):

    _description = _(__doc__)
    _name = 'tour.tour'
    _inherit = ['mail.thread']

    name = fields.Char('Tour name', required=True)
    state = fields.Selection(
        [
            ('draft', _('Draft')),
            ('open', _('Saved')),
            ('booking', _('In Reservation')),
            ('reserved', _('Reserved')),
            ('confirmed', _('Confirmed')),
            ('done', _('Closed')),
        ],
        'Status',
        readonly=True,
        default='draft',
    )
