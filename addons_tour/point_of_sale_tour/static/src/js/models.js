odoo.define('point_of_sale_tour.models', function (require) {
    "use strict";

    var PosModels = require('point_of_sale.models');
    var Model = require('web.Model');

    /* Load required fields into res_partner */
    //PosModels.load_fields("res.partner",['property_account_position_id'])

    /* ********************************************************
     Updating point_of_sale.PosModel
     ******************************************************** */

    var _super_posmodel = PosModels.PosModel.prototype;
    PosModels.PosModel = PosModels.PosModel.extend({
        asignar_campo_a_orden: function (campo, valor) {
            var order = _super_posmodel.get_order.call(this);
            if (campo && order) {
                switch (campo) {
                    case 'boarding':
                        order.set_boarding(valor);
                        break;
                    case 'boarding_date':
                        order.set_boarding_date(valor);
                        break;
                    case 'boarding_time':
                        order.set_boarding_time(valor);
                        break;
                }
            }
        },
    });

    /* ********************************************************
    Updating point_of_sale.Order
    ******************************************************** */
    var _super_order = PosModels.Order.prototype;
    PosModels.Order = PosModels.Order.extend({
        export_as_JSON: function() {
            var json = _super_order.export_as_JSON.apply(this,arguments);
            json.boarding = this.boarding;
            json.boarding_date = this.boarding_date;
            json.boarding_time = this.boarding_time;
            return json;
        },
        init_from_JSON: function(json) {
            _super_order.init_from_JSON.apply(this,arguments);
            this.boarding = json.boarding;
            this.boarding_date = json.boarding_date;
            this.boarding_time = json.boarding_time;
        },
        export_for_printing: function() {
            var json = _super_order.export_for_printing.apply(this,arguments);
            json.boarding = this.boarding;
            json.boarding_date = this.boarding_date;
            json.boarding_time = this.boarding_time;
            return json;
        },
        set_tour_category_id: function(tour_category_id){
            if (tour_category_id) {
                this.tour_category_id = tour_category_id;
            }
            this.save_to_db();
        },
        get_tour_category_id: function(){
            return this.tour_category_id;
        },
        set_boarding: function(value) {
            if (value) {
                this.boarding = value;
            }
            this.save_to_db();
        },
        get_boarding: function(){
            return this.boarding;
        },
        set_boarding_date: function(valor) {
            if (valor) {
                this.boarding_date = valor;
            }
            this.save_to_db();
        },
        get_boarding_date: function(){
            return this.boarding_date;
        },
        set_boarding_time: function (valor){
            if (valor) {
                this.boarding_time = valor;
            }
            this.save_to_db();
        },
        get_boarding_time: function(){
            return this.boarding_time;
        },
    });

});
