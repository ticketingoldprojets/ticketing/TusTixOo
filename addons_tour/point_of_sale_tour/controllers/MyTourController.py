# -*- coding: utf-8 -*-

import base64
import os

from odoo import http
from odoo.http import content_disposition, dispatch_rpc, request, \
                      serialize_exception as _serialize_exception


def binary_content(xmlid=None, model='ir.attachment', id=None, field='datas', unique=False, filename=None, filename_field='datas_fname', download=False, mimetype=None, default_mimetype='application/octet-stream', env=None):
    return request.registry['ir.http'].binary_content(
        xmlid=xmlid, model=model, id=id, field=field, unique=unique, filename=filename, filename_field=filename_field,
        download=download, mimetype=mimetype, default_mimetype=default_mimetype, env=env)

class MyController(http.Controller):
    def force_contenttype(self, headers, contenttype='image/png'):
        dictheaders = dict(headers)
        dictheaders['Content-Type'] = contenttype
        return dictheaders.items()

    def placeholder(self, image='11.png'):
        addons_path = http.addons_manifest['point_of_sale_tour']['addons_path']
        result = open(os.path.join(addons_path, 'point_of_sale_tour', 'demo', 'img', image), 'rb').read()
        return result

    @http.route('/web/tour_img', type='http', auth='public')
    def content_image(self, xmlid=None, model='ir.attachment', id=None, field='datas', filename_field='datas_fname', unique=None, filename=None, mimetype=None, download=None, width=0, height=0):
        status, headers, content = binary_content(xmlid=xmlid, model=model, id=id, field=field, unique=unique, filename=filename, filename_field=filename_field, download=download, mimetype=mimetype, default_mimetype='image/png')

        image_base64 = self.placeholder(image=id+'.png')  # could return (contenttype, content) in master
        headers = self.force_contenttype(headers, contenttype='image/png')

        response = request.make_response(image_base64, headers)
        response.status_code = status
        return response
