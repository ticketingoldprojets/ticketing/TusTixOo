odoo.define('point_of_sale_tour.chrome', function (require) {
    "use strict";

    var core = require('web.core');
    var _t = core._t;

    var chrome = require('point_of_sale.chrome');

    chrome.Chrome.include({
        renderElement: function(){
            this._super();
            this.$('#pos-tour-data').click(this.show_tour_data_menu.bind(this));
        },
        show_tour_data_menu: function () {
            var self = this;
            self.pos.gui.show_popup('selection', {
                title: _t('Tour data'),
                list: self.get_tour_data_menu_options(self),
                confirm: function (item) {
                    if (item === 'boarding') {
                        self.action_boarding(self);
                    } else if (item === 'boarding_date') {
                        self.action_boarding_date(self);
                    } else if (item === 'boarding_time') {
                        self.action_boarding_time(self);
                    }
                },
                cancel: function () {
                    // user chose nothing
                }
            });
        },
        get_tour_data_menu_options: function(self){

            var boarding = self.get_boarding();
            boarding = boarding ? (' [' + boarding  +']') : '';
            var boarding_time = self.get_boarding_time();
            boarding_time = boarding_time ? (' [' + boarding_time  +']') : '';

            var option_list = [
               { label: _t('Boarding') + boarding,  item: 'boarding' },
               { label: _t('Boarding Date'),  item: 'boarding_date' },
               { label: _t('Boarding Time') + boarding_time ,  item: 'boarding_time' },
            ];

            return option_list;
        },
        action_boarding: function(self){
            self.pos.gui.show_popup('selection', {
                title: _t('Boarding'),
                list: self.get_boarding_options(self),
                confirm: function (item) {
                    self.confirm_boarding(item);
                    self.show_tour_data_menu();
                },
                cancel: function () {
                    self.show_tour_data_menu();
                }
            });
        },
        get_boarding: function(){
            return this.pos.get_order().get_boarding();
        },
        confirm_boarding: function(item){
            this.pos.get_order().set_boarding(item);
        },
        get_boarding_options: function(self){
            var tour_categ_id = this.pos.get_order().get_tour_category_id();

            return this.pos.db.get_tour_category_boardings_by_id(tour_categ_id);
        },
        action_boarding_date: function(self){
            self.gui.show_popup('number',{
                'title': 'Boarding Date',
                // 'value': self.pos.get_establishment(),
                'confirm': function(value) {
                    value = Math.max(1,Number(value));
                    self.show_tour_data_menu();
                },
                cancel: function(){
                    self.show_tour_data_menu();
                }
            });
        },
        action_boarding_time: function(self){
            self.pos.gui.show_popup('selection', {
                title: _t('Boarding Time'),
                list: self.get_boarding_time_options(self),
                confirm: function (item) {
                    self.confirm_boarding_time(item);
                    self.show_tour_data_menu();
                },
                cancel: function () {
                    self.show_tour_data_menu();
                }
            });
        },
        get_boarding_time_options: function(self){

            var option_list = [
               { label: _t('09:00 h') ,  item: 'h09' },
               { label: _t('12:00 h') ,  item: 'h12' },
               { label: _t('15:00 h') ,  item: 'h15' },
               { label: _t('20:00 h') ,  item: 'h20' },
               { label: _t('23:00 h') ,  item: 'h23' },
            ];

            return option_list;
        },
        get_boarding_time: function(){
            return this.pos.get_order().get_boarding_time();
        },
        confirm_boarding_time: function(item){
            this.pos.get_order().set_boarding_time(item);
        },
    });
});

