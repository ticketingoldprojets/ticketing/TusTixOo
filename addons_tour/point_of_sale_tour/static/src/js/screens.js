odoo.define('point_of_sale_tour.screens', function (require) {
"use strict";

var PosBaseWidget = require('point_of_sale.BaseWidget');
var gui = require('point_of_sale.gui');
var models = require('point_of_sale.models');
var core = require('web.core');
var Model = require('web.DataModel');
var utils = require('web.utils');
var formats = require('web.formats');

var QWeb = core.qweb;
var _t = core._t;

var datepicker = require('web.datepicker');

var round_pr = utils.round_precision;

var screens = require('point_of_sale.screens');

function set_node_hidden(nd, hidden){
    if (nd && nd.classList){
        if (hidden){
            nd.classList.add('oe_hidden');
        } else {
            nd.classList.remove('oe_hidden');
        }
    }
}

/* --------- The Tour Category List --------- */

var TourCategoryListWidget = PosBaseWidget.extend({
    template:'TourCategoryListWidget',
    init: function(parent, options) {
        var self = this;
        this._super(parent,options);

        this.category_list = [];
        this.category_cache = new screens.DomCache();

        this.set_category_list(this.get_category_list());

        this.click_product_handler = function(){
            self.set_category(this.dataset.tourCategoryId);
            // var product = self.pos.db.get_product_by_id(this.dataset.productId);
            // options.click_product_action(product);
        };

        this.tree_selected_categories = [];
    },
    init_category_list: function () {
        this.set_category_list(this.get_category_list());
    },
    get_category_list: function (node_id) {
        var self = this;
        self.tree_selected_categories = [];
        var res = this.pos.db.get_tour_category_list();

        if (node_id && res){
            var idx = res.findIndex(function(val){return val && val.id == node_id;});
            if (idx >= 0) {
                self.tree_selected_categories.push(res[idx]);
                res = (idx >= 0) && res[idx].childs || res;
            } else {
                res.forEach(function(val){
                    if (val && val.childs){
                        idx = val.childs.findIndex(function(val){return val && val.id == node_id;});
                        res = (idx >= 0) && val.childs[idx].childs || [];
                        if (idx >= 0){
                            self.tree_selected_categories.push(val);
                            self.tree_selected_categories.push(val.childs[idx]);
                        }
                    }
                })
            }
        }

        return res;
    },
    set_category_list: function(category_list){
        this.category_list = category_list;
        this.renderElement();
    },
    set_category: function(category_id){
        this.pos.get_order().set_tour_category_id(category_id);
        this.set_category_list(this.get_category_list(category_id));

        var parent = this.getParent();
        if (parent && parent.product_categories_widget){
            parent.product_categories_widget.set_tour_categories(this.tree_selected_categories);
        }
    },
    get_product_image_url: function(product){
        var res = window.location.origin + '/web/tour_img?id='+product.id;
        return res;
    },
    replace: function($target){
        this.renderElement();
        var target = $target[0];
        if (target && target.parentNode) {
            target.parentNode.replaceChild(this.el, target);
        }
    },
    render_category: function(category){
        var cached = this.category_cache.get_node(category.id);
        if(!cached){
            var product_html = QWeb.render('TourCategory',{
                    widget:  this,
                    product: category,
                    image_url: this.get_product_image_url(category),
                });
            var product_node = document.createElement('div');
            product_node.innerHTML = product_html;
            product_node = product_node.childNodes[1];
            this.category_cache.cache_node(category.id,product_node);
            return product_node;
        }
        return cached;
    },

    renderElement: function() {
        var self = this;
        var el_str = QWeb.render(this.template, {widget: this});
        var el_node = document.createElement('div');
        el_node.innerHTML = el_str;
        el_node = el_node.childNodes[1];

        if (this.el && this.el.parentNode) {
            this.el.parentNode.replaceChild(el_node, this.el);
        }
        this.el = el_node;

        var category_list = this.category_list;
        if (category_list && category_list.length > 0) {
            var list_container = el_node.querySelector('.tour-category-list');
            if (list_container) {
                for (var i = 0, len = category_list.length; i < len; i++) {
                    var product_node = this.render_category(category_list[i]);
                    product_node.addEventListener('click', this.click_product_handler);
                    list_container.appendChild(product_node);
                }
            }
        } else {
            if (this.getParent()){
                this.getParent().show_product_list();
            }
        }
    },
});

screens.ProductScreenWidget.include({
    start : function() {
        var self = this;

        this.tour_category_list_widget = new TourCategoryListWidget(this,{
             // click_product_action: function(product){ self.click_product(product); },
             // product_list: this.pos.db.get_product_by_category(0)
        });
        this.tour_category_list_widget.replace(this.$('.placeholder-ProductListWidget'));

        this._super();
    },
    show_tour_category_list: function () {
        var el_node = this.el;
        var nd = el_node.querySelector('#row_products');
        set_node_hidden(nd, true);
        if (this.product_categories_widget){
            this.product_categories_widget.set_categories_hidden(true);
        }

        nd = this.el.querySelector('#row_category_list');
        if (nd && this.tour_category_list_widget){
            this.tour_category_list_widget.init_category_list();
            set_node_hidden(nd, false);
        }
    },
    show_product_list: function () {
        var el_node = this.el;
        var product_list_widget = this.product_list_widget;
        if (el_node && product_list_widget){

            product_list_widget.replace(el_node.querySelector(".placeholder-ProductListWidget"));
            set_node_hidden(el_node.querySelector('#row_products'), false);
            set_node_hidden(el_node.querySelector('.searchbox'), false);
            set_node_hidden(el_node.querySelector('#row_category_list'), true);
        }
    }
});

screens.ProductCategoriesWidget.include({
    init: function(parent, options){
        this.tour_categories = [];
        this._super(parent, options);
        this.searchboxIsHidden = true;
    },
    set_tour_categories: function (tour_categories) {
        var self = this;
        self.tour_categories.splice(0, self.tour_categories.length);
        if (tour_categories){
            tour_categories.forEach(function (item) {
                self.tour_categories.push(item);
            })
        }
        this.renderElement();
    },
    set_category : function(category){
        this._super(category);
        var parent = this.getParent();
        if (!category && parent && parent.show_tour_category_list){
            parent.show_tour_category_list();
            this.tour_categories = [];
        }
    },
    renderElement: function(){
        this._super();
        var el_node = this.el;
        set_node_hidden(el_node.querySelector('.searchbox'), this.searchboxIsHidden);

        var parent = this.getParent();
        if (parent && parent.tour_category_list_widget) {
            var buttons = el_node.querySelectorAll('.js-tour-category-switch');
            for (var i = 0; i < buttons.length; i++) {
                buttons[i].addEventListener('click', parent.tour_category_list_widget.click_product_handler);
            }
        }
    },
    set_categories_hidden: function (hidden) {
        this.searchboxIsHidden = hidden;
    },
});

screens.ProductListWidget.include({
    replace: function ($target) {
        this.renderElement();
        var target = $target[0];
        if (!target){
            target = $target;
        }
        if (target && target.parentNode){
            target.parentNode.replaceChild(this.el, target);
        }
    },
    renderElement: function() {
        var parent = this.getParent();
        if (parent && parent.tour_category_list_widget) {
            var tour_category_list = parent.tour_category_list_widget.tree_selected_categories;
            if (tour_category_list && tour_category_list.length > 0) {
                var categ_id = tour_category_list[tour_category_list.length - 1].id;
                var price;

                // Tuve que reiniciar la cache de productos para obligar a seleccionar el producto desde la
                // lista actualizada, llenando nuevamente la cache.
                // Como la lista de productos es pequeña, considero que no es tan necesaria una cache.
                // Esta solución hace INUTIL la cache, pero era necesario para poder cambiar el precio
                // en tiempo de ejecución.
                this.product_cache = new screens.DomCache();
                //***

                for (var i = 0, len = this.product_list.length; i < len; i++) {
                    price = this.pos.db.get_tour_price(categ_id, this.product_list[i].id);
                    this.product_list[i].price = price;
                    this.product_list[i].list_price = price;
                }
            }
        }
        this._super();
    },
});


return {
    TourCategoryListWidget: TourCategoryListWidget,
};

});

