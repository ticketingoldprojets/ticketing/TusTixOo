# -*- coding: utf-8 -*-

{
    'name': 'Point of Sale Extension for Tours',
    'version': '10.0.1.0.1',
    'depends': [
        'base',
        'point_of_sale',
    ],
    'author': "Eduardo Barea Bermudez",
    'license': "",
    'summary': '''''',
    'website': '',
    'data': [
        'views/point_of_sale_tour_assets.xml',
    ],
    'demo': [],
    'qweb': ['static/src/xml/pos.xml'],
    'installable': True,
}
